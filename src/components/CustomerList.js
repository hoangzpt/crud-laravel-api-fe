import React, { Component } from "react";
import Customer from "./Customer";

class CustomerList extends Component {
    onDelete = id => {
        // console.log("customer list ", id);
        this.props.onDelete(id);
    };

    onEdit = data => {
        // console.log("customer list ", id);
        this.props.onEdit(data);
    };

    render() {
        const customers = this.props.customers;
        return (
            <div className="data">
                <h1>Customer List</h1>
                <table className="ui celled table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        {customers.map(customer => {
                            return <Customer 
                                    customer={customer} 
                                    key={customer.id} 
                                    onDelete={this.onDelete} 
                                    onEdit={this.onEdit}
                                    />;
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default CustomerList;